# Evaluacion 3

# Team Web - Testing E2E

## Members: César Soto - Christopher Alarcón

This project contains e2e (end to end) tests for the web http://pruebas-soft.s3-website.us-east-2.amazonaws.com
.

## Tests 🧪

Login

- Successful login with verified user. ✅
- Invalid credentials. ✅

Club

- Club details. ✅
- Redirect to /login page when token is not provided. ✅
- Add Member. ✅
- Fail in add member because it doesnt have an email. ✅
- Without action when pressing the delete member button. ✅

Home

- Redirect to /login page when token is not provided. ✅
- Home correct. ✅
- Get list clubs. ✅
- Add club. ✅
- Error when adding a club due to a missing name. ✅

## Initialization 💻

Install all packages

```
npm i
```

## Run Tests 🚀

To run the UI Cypress:

```
npm run open
```

To run the tests in the console:

```
npm run e2e
```

## Environment 🌿

- You have to set an ".env" file containing the following test credentials:

```
USER="c.soto37@ufromail.cl"
PASSWORD="EvlroSZf"
```

## Recommended Browsers for Cypress 🌐

- Chrome 🌎 ✅.
- Edge 🌊 ✅.
- Electron 🖥️ ✅.
- Firefox 🦊⚠️ (It may cause some issues).

## Video Demo

https://drive.google.com/file/d/1adDY3fx0HK4siS_rW4POU2TI8LS2b5t_/view?usp=sharing
