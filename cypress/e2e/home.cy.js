describe("Home", () => {
  it("[Error H-1] Redirect to /login page when token is not provided", () => {
    cy.visit("/");
    cy.url().should(
      "eq",
      "http://pruebas-soft.s3-website.us-east-2.amazonaws.com/login"
    );
  });

  it("[Successful H-2] Home correct", () => {
    cy.login().then(() => {
      cy.visit("/", {
        failOnStatusCode: false,
      });
      cy.get("h2")
        .should("contain", "Welcome CÉSAR,")
        .wait(1000)
        .and("be.visible");
    });
  });

  it("[Successful H-3] Get list clubs", () => {
    cy.login().then((token) => {
      cy.getClubs(token).then((clubes) => {
        cy.visit("/", {
          failOnStatusCode: false,
        });
        cy.get(`#${clubes[0]._id}`)
          .should("exist")
          .wait(1000)
          .and("be.visible")
          .and("contain", clubes[0].name);
      });
    });
  });

  it("[Successful H-4] Add club", () => {
    cy.login().then((token) => {
      cy.getClubs(token).then((clubes) => {
        cy.visit("/", {
          failOnStatusCode: false,
        });
        const tamañoAnterior = clubes.length;
        const clubName = "Name Test";
        cy.get('[data-v-31e950ad=""]').contains("Add Club").click();
        cy.get('[aria-label="Club name"]').type("Name Test");
        cy.get('[aria-label="Club description"]').type("Descripcion Test");
        cy.get(":nth-child(2) > .q-btn__content > .block").click();
        cy.getClubs(token).then((clubes) => {
          cy.visit("/", {
            failOnStatusCode: false,
          });
          cy.get(`div[id=${clubes[clubes.length - 1]._id}]`)
            .click()
            .wait(1000);
          cy.get(".text-h3")
            .contains(clubes[clubes.length - 1].name)
            .should("have.text", clubName);
          expect(clubes.length).to.equal(tamañoAnterior + 1);
        });
      });
    });
  });

  it("[Successful H-5] Error when adding a club due to a missing name.", () => {
    cy.login().then(() => {
      cy.visit("/", {
        failOnStatusCode: false,
      });
      cy.get('[data-v-31e950ad=""]').contains("Add Club").click();
      cy.get('[aria-label="Club description"]').type("Descripcion");
      cy.get(":nth-child(2) > .q-btn__content > .block").click();
      cy.get("p.text-negative.text-center.error")
        .should("be.visible")
        .and("have.text", "name is required");
    });
  });
});
