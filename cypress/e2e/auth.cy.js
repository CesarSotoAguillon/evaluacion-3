describe("Login", () => {
  it("[Successful L-1] Successful login with verified user.", () => {
    cy.visit("/login", {
      failOnStatusCode: false,
    });
    cy.get('input[id="login-email"').type(Cypress.env("USER"));
    cy.get('input[id="login-password"').type(Cypress.env("PASSWORD"));
    cy.get("button").click();
    cy.url().should("eq","http://pruebas-soft.s3-website.us-east-2.amazonaws.com/");
  });
  it("[Error L-2] Invalid credentials", () => {
    cy.visit("/login", {
      failOnStatusCode: false,
    });
    cy.get('input[id="login-email"').type("correo@incorrecto.cl");
    cy.get('input[id="login-password"').type("correo");
    cy.get("button").click();
    cy.get(".text-negative").should("text", "invalid credentials");
  });
});
