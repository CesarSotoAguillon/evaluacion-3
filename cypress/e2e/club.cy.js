import { faker } from "@faker-js/faker";

const name = faker.person.firstName();
const lastName = faker.person.lastName();
const email = faker.internet.email();
const DNI = faker.number.int();
const nickname = faker.internet.userName();

describe("Club", () => {
  // Antes de logearse
  it("[ERROR C-2] Redirect to /login page when token is not provided", () => {
    cy.visit("/club", {
      failOnStatusCode: false,
    });
    cy.url().should(
      "eq",
      "http://pruebas-soft.s3-website.us-east-2.amazonaws.com/login"
    );
  });
  it("[SUCCESS C-1] Club details", () => {
    cy.login().then((token) => {
      cy.getClubs(token).then((clubes) => {
        cy.visit("/", {
          failOnStatusCode: false,
        });
        cy.get(`div[id=${clubes[0]._id}]`).click();
        cy.get('span[class="text-h3"]').contains(clubes[0].name);
      });
    });
  });

  it("[SUCCESS C-3] Add Member", () => {
    cy.login().then((token) => {
      cy.getClubs(token).then((clubes) => {
        cy.visit("/", {
          failOnStatusCode: false,
        });
        cy.get(`div[id=${clubes[0]._id}]`).click().wait(1000);
        let membersCount;

        cy.get("div.text-h6")
          .invoke("text")
          .then((text) => {
            const regex = /\((\d+)\)/;
            const match = text.match(regex);

            if (match && match.length > 1) {
              membersCount = parseInt(match[1]);
            }
          });
        cy.get(
          ".q-markup-table.q-table__container.q-table__card.q-table--horizontal-separator.q-table--no-wrap table.q-table tbody tr"
        )
          .its("length")
          .then((initialRowCount) => {
            cy.get("button.q-btn-item.bg-secondary.text-white")
              .contains("New member")
              .click();
            cy.get('[aria-label="Member name*"]').type(`${name}`);
            cy.get('[aria-label="Member lastName*"]').type(`${lastName}`);
            cy.get('[aria-label="Member email*"]').type(`${email}`);
            cy.get('[aria-label="Member DNI"]').type(`${DNI}`);
            cy.get('[aria-label="Member Nickname"]').type(`${nickname}`);
            cy.get(".text-primary > .q-btn__content").click();

            cy.get(".q-notification__message")
              .should("contain", "Member added successfully")
              .wait(2000);
            cy.reload().wait(2000);
            // Deberia comparar el numero de miembros antes y despues de agregar uno
            cy.get("div.text-h6")
              .invoke("text")
              .then((text) => {
                const regex = /\((\d+)\)/;
                const match = text.match(regex);

                if (match && match.length > 1) {
                  const membersCount2 = parseInt(match[1]);
                  expect(membersCount2).to.equal(membersCount + 1);
                }
              });
            // Deberia comparar el largo de la tabla antes y despues de agregar un miembro
            cy.get(
              ".q-markup-table.q-table__container.q-table__card.q-table--horizontal-separator.q-table--no-wrap table.q-table tbody tr"
            )
              .its("length")
              .should("eq", initialRowCount + 1);
          });
      });
    });
  });
  it("[ERROR C-4] Fail in add member because it doesnt have an email", () => {
    cy.login().then((token) => {
      cy.getClubs(token).then((clubes) => {
        cy.visit("/", {
          failOnStatusCode: false,
        });
        cy.get(`div[id=${clubes[0]._id}]`).click().wait(1000);

        cy.get("button.q-btn-item.bg-secondary.text-white")
          .contains("New member")
          .click();
        cy.get('[aria-label="Member name*"]').type(`${name}`);
        cy.get('[aria-label="Member lastName*"]').type(`${lastName}`);

        cy.get('[aria-label="Member DNI"]').type(`${DNI}`);
        cy.get('[aria-label="Member Nickname"]').type(`${nickname}`);
        cy.get(".text-primary > .q-btn__content").click().wait(2000);
        cy.contains("email is required and must be a valid email")
          .should("have.class", "text-negative")
          .should("have.class", "text-center");
      });
    });
  });

  it("[ERROR C-5] Without action when pressing the delete member button", () => {
    cy.login().then((token) => {
      cy.getClubs(token).then((clubes) => {
        cy.visit("/", {
          failOnStatusCode: false,
        });
        cy.get(`div[id=${clubes[0]._id}]`).click().wait(1000);
        cy.get(':nth-child(1) > [for="actions"] > .text-negative > .q-btn__content > .q-icon').click().wait(2000);
        cy.get(".q-notification__message").should("contain", "Unavailable");
      });
    });
  });
});
